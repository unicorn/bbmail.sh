#!/bin/sh
# Copyright © 2021 Edin Taric, This software is licensed under the MIT
# license. You should have received a copy of that license in the README
# file along with this software.
########################################################################
########################## CONFIGURATION ###############################
########################################################################

# Sender name and email address
from_name="My Name"
from_addr="myname@example.com"

# Recipient email address
to_addr="recipient@example.org"

# Default email subject. This is used if no custom subject was passed
# to this script as an argument when running it. The below example would
# look like this:
#   "2021-10-20 My Name command output"
subject_default="$(date '+%Y-%m-%d') $from_name command output"

# Username and password to use for SMTP authentication
user="myusername"
pass="supersecretpassword"

# SMTP server and port to connect to.
smtp_host="smtp.example.com:587"

# Connection type to use with the server. This has to be either "TLS" or
# "STARTTLS".
conn_type="STARTTLS"

# Changing this to 1 turns on verbose output, which can be useful if
# there is an issue, since bbmail.sh is completely silent by default.
debug=0

########################################################################
########################################################################
########################################################################


subject="${1:-$subject_default}"
mainout=/tmp/bbmail.out
partout=/tmp/bbmail.part
rm $mainout 2> /dev/null
rm $partout 2> /dev/null

if [ $conn_type = "STARTTLS" ]; then
	openssl="openssl s_client -quiet -starttls smtp -connect $smtp_host" 
elif [ $conn_type = "TLS" ]; then
	openssl="openssl s_client -quiet -connect $smtp_host"
else
	echo 'Configuration error: "conn_type" must be either "TLS" or "STARTTLS"' 1>&2
	exit 1
fi

if [ $debug -eq 1 ]; then
	sout=/dev/stdout
	eout=/dev/stderr
	verbose="-v"
else
	sout=/dev/null
	eout=/dev/null
	verbose=
fi


cat >$mainout <<EOF
From: $from_name <$from_addr>
To: $to_addr
Date: $(date -R)
Content-Type: text/plain; charset="UTF-8"
MIME-Version: 1.0
Subject: $subject

EOF

while read line
do
	printf %s\\n "$line" >>$partout
done < "/dev/stdin"

if [ -s $partout ]; then
	cat $partout >>$mainout
	cat $mainout | busybox sendmail $verbose -H "$openssl" -f "$from_addr" \
		-amPLAIN -au$user -ap$pass $to_addr >$sout 2>$eout \
		&& rm $mainout && rm $partout
fi
